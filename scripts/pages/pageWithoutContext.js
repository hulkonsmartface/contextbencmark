/* 
		You can modify its contents.
*/
const extend = require('js-base/core/extend');
const PageWithoutContextDesign = require('ui/ui_pageWithoutContext');
const Button = require("sf-core/ui/button");
const runner = require("../benchmark/runner.js");
const PageBase = require('sf-core/ui/page');
const styler = require("@smartface/styler/lib/styler");
const themeStyler = styler(require("../themes/benchmarkTheme.json"));
const sfCorePropFactory = require("@smartface/contx/lib/smartface/sfCorePropFactory").default;
const Label = require("sf-core/ui/label");
var stop;

const PageWithoutContext = extend(PageWithoutContextDesign)(
  // Constructor
  function(_super) {
    // Initalizes super class for this page scope
    _super(this);
    stop = runner.start();

    // Overrides super.onShow method
    this.onShow = onShow.bind(this, this.onShow.bind(this));
    
    // Overrides super.onLoad method
    this.onLoad = onLoad.bind(this, this.onLoad.bind(this));
  });

/**
 * @event onShow
 * This event is called when a page appears on the screen (everytime).
 * @param {function} superOnShow super onShow function
 * @param {Object} parameters passed from Router.go function
 */
function onShow(superOnShow, data) {
  superOnShow();
  // console.log(JSON.stringify(data));
  // runner.add(createButtonsWithoutStyle.bind(this), "without context and style");
  // runner.add(createButtonsWithStyle.bind(this), "without context with style");
  runner.add(createButtonsinContextWithStyle.bind(this), "in context with style");
  // runner.add(createButtonsinContextWithoutStyle.bind(this), "in context without style");

  // console.log("stop : "+stop());

  runner.runAll(1, function(res){
    console.log(res[0].asString);
  });
}

const max = 1000;
const Component = Label;

function createButtonsWithStyle(){
  for (var i = 0; i < max; ++i) {
    var button = new Component();
    this.layout.addChild(button);
    var style = themeStyler(".button")();
    Object.assign(button, sfCorePropFactory(style));
  }
}

function createButtonsWithoutStyle(){
  for (var i = 0; i < max; ++i) {
    var button = new Component();
    this.layout.addChild(button);
  }
}

function createButtonsinContextWithStyle(){
  for (let i = 0; i < max; ++i) {
    this.layout.addChild(new Component(), 'button'+i, ".button");
  }
}

function createButtonsinContextWithoutStyle(){
  for (let i = 0; i < max; ++i) {
    this.layout.addChild(new Component(), 'button'+i, "");
  }
}

/**
 * @event onLoad
 * This event is called once when page is created.
 * @param {function} superOnLoad super onLoad function
 */
function onLoad(superOnLoad) {
  const page = this;
  superOnLoad();
}

module.exports = PageWithoutContext;
