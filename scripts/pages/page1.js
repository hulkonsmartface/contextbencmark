const extend = require("js-base/core/extend");
const Router = require("sf-core/ui/router");
const System = require("sf-core/device/system");

// Get generated UI code
const Page1Design = require("ui/ui_page1");

const Page1 = extend(Page1Design)(
    // Constructor
    function(_super) {
        // Initalizes super class for this page scope
        _super(this);
        // Overrides super.onShow method
        this.onShow = onShow.bind(this, this.onShow.bind(this));
        // Overrides super.onLoad method
        this.onLoad = onLoad.bind(this, this.onLoad.bind(this));
    });

/**
 * @event onShow
 * This event is called when a page appears on the screen (everytime).
 * @param {function} superOnShow super onShow function
 * @param {Object} parameters passed from Router.go function
 */
function onShow(superOnShow) {
    const page = this;
    superOnShow();

    if (System.OS === "Android") {
        setTimeout(() => page.btn2.enabled = true, 300);
    }
}

/**
 * @event onLoad
 * This event is called once when page is created.
 * @param {function} superOnLoad super onLoad function
 */
function onLoad(superOnLoad) {
    const page = this;
    superOnLoad();

    page.headerBar.leftItemEnabled = false;
    page.btn.onPress = btn1_onPress.bind(page);
    page.btn2.onPress = btn2_onPress.bind(page);
}



// Gets/sets press event callback for btn
function btn1_onPress() {
    Router.go("pageWithoutContext", "out");

}

function btn2_onPress() {
    Router.go("pageWithoutContext", "in");
}

module.exports = Page1;
