const extend = require("js-base/core/extend");
const Router = require("sf-core/ui/router");
const Color = require("sf-core/ui/color");
const System = require("sf-core/device/system");
const Button = require("sf-core/ui/button");


// Get generated UI code
const Page2Design = require('ui/ui_pageWithContext');

const Page2 = extend(Page2Design)(
    // Constructor
    function(_super) {
        // Initalizes super class for this page scope
        _super(this);
        // Overrides super.onShow method
        this.onShow = onShow.bind(this, this.onShow.bind(this));
        // Overrides super.onLoad method
        this.onLoad = onLoad.bind(this, this.onLoad.bind(this));
    });

/**
 * @event onShow
 * This event is called when a page appears on the screen (everytime).
 * @param {function} superOnShow super onShow function
 * @param {Object} parameters passed from Router.go function
 */
function onShow(superOnShow, data) {
    const page = this;
    superOnShow();

    page.headerBar.itemColor = Color.BLACK;

}

/**
 * @event onLoad
 * This event is called once when page is created.
 * @param {function} superOnLoad super onLoad function
 */
function onLoad(superOnLoad) {
    const page = this;
    superOnLoad();

    if (!page.dispatch)
    throw "Page must be connected to the context"
    for (let i = 0; i < 1000; ++i)
    page.layout.addChild(new Button(), `button${i}`, ".button", userProps => {
        userProps.width = 250;
        userProps.height = 10;
        // more props maybe
        return userProps;
    });
}



module.exports = Page2;
